from flask.ext.appbuilder import Model
from flask.ext.appbuilder.models.mixins import AuditMixin, FileColumn, ImageColumn
from sqlalchemy import Column, Integer, String, ForeignKey, Date
from sqlalchemy.orm import relationship

"""

You can use the extra Flask-AppBuilder fields and Mixin's

AuditMixin will add automatic timestamp of created and modified by who


"""


# ContactGroup
class Deals(AuditMixin, Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True, nullable=False)
    currency = Column(String(50), unique=False, nullable=False)
    fund = Column(String(50), unique=False, nullable=False)

    def __repr__(self):
        return self.name


# Contact
class Cashflows(AuditMixin, Model):
    id = Column(Integer, primary_key=True)
    type = Column(String(50), unique=False, nullable=False)
    birthday = Column(Date)
    cashflows = Column(Integer, unique=False, nullable=False)
    deals_id = Column(Integer, ForeignKey('deals.id'), nullable=False)
    deals = relationship("Deals")

    def __repr__(self):
        return self.name
