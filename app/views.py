from flask import render_template
from flask.ext.appbuilder.models.sqla.interface import SQLAInterface
from flask.ext.appbuilder import ModelView
from app import appbuilder, db
from app.models import Deals, Cashflows

"""
    Create your Views::


    class MyModelView(ModelView):
        datamodel = SQLAInterface(MyModel)


    Next, register your Views::


    appbuilder.add_view(MyModelView, "My View", icon="fa-folder-open-o", category="My Category", category_icon='fa-envelope')
"""


class DealsModelView(ModelView):
    datamodel = SQLAInterface(Cashflows)
    label_columns = {'id': 'CF_ID', 'deals_id': 'Deal_ID', 'deals_group': 'Deals', 'type': 'CF_Type',
                     'cashflows': 'Cashflows', 'birthday': 'Value_Date'}
    list_columns = ['id', 'deals_id', 'birthday', 'type', 'cashflows', ]

    show_fieldsets = [
        ('Summary', {'fields': ['type', 'cashflows', 'deals_id']})
    ]


class GroupModelView(ModelView):
    datamodel = SQLAInterface(Deals)
    related_views = [DealsModelView]
    label_columns = {'id': 'Deal_ID', 'name': 'Deal_Name', 'fund': 'Fund', 'currency': 'Local_Currency'}
    list_columns = ['id', 'name', 'currency', 'fund', ]


"""
    Application wide 404 error handler
"""


@appbuilder.app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', base_template=appbuilder.base_template, appbuilder=appbuilder), 404


db.create_all()

appbuilder.add_view(GroupModelView, "List Deals", icon="fa-folder-open-o", category="Client Portfolio",
                    category_icon="fa-envelope")
appbuilder.add_view(DealsModelView, "List Cashflows", icon="fa-envelope", category="Client Portfolio")
