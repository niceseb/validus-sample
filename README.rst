Start your application
----------------------

- Install it::

	git clone https://niceseb@bitbucket.org/niceseb/validus-sample.git

- Run it::
    mkvirtualenv venv
    pip install -r requirements.txt
    fabmanager create-admin
    fabmanager run
    Open browser and it will be on localhost:8080


That's it!!

